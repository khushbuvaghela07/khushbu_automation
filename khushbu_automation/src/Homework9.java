public class Homework9 {

    public static void main(String[] args) {
        BaseElement base = new BaseElement();
        LinkElement link = new LinkElement();
        TextElement text = new TextElement();

        base.getElement();
        base.click();

        link.getElement();
        link.click();

        text.getElement();
        text.click();

    }
}

