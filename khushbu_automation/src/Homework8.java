public class Homework8 {

    public static void main(String[] args) {

        BankingAccount myAccount = new BankingAccount();

        //Print current balance
        System.out.println("Current balance is : $" + myAccount.balance);

        //Deposit
        myAccount.deposit(500);
        myAccount.deposit(150);
        myAccount.deposit(35);

        //Print current balance after deposit
        System.out.println("Current balance after deposit is : $" + myAccount.balance);

        //Withdrawal
        myAccount.withdrawal(40);
        myAccount.withdrawal(120);

        //Print current balance after withdrawal
        System.out.println("Current balance after withdrawal is : $" + myAccount.balance);
    }
}

