package Homework10;

public class Main {
    public static void main(String args[]){
        BankAccount account = new BankAccount();

        int balance = account.getBalance();
        System.out.println("Current balance is $" +balance);

        //Check if the customer deposits $500, $150 and $35
        account.setBalance(500);
        account.setBalance(150);
        account.setBalance(35);

        //withdraws -$40 and -$120
        account.setBalance(-40);
        account.setBalance(-120);

        //Check if additionally customer attempts to withdraw -$900 (over the balance), transaction fails
        account.setBalance(-900);
    }
}
