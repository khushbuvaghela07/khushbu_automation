package Homework10;

public class BankAccount {
    private int balance = 0;

    public int getBalance() {
        return balance;
    }

    public void setBalance(int accountBalance) {

        //Check if balance is negative. If true than fail the transaction or else make the transaction.
        if(balance + accountBalance < 0){
            System.out.println("Withdrawal of $" + Math.abs(accountBalance) + " cannot be completed. Your balance is $" +balance+" .");
        }else{
            balance = balance + accountBalance;
            System.out.println("Your balance has changed by $" + accountBalance+" and now it is: $"+balance+".");
        }
    }

}
