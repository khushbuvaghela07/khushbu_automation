package Homework13;
import java.util.HashMap;

public class Map {

    public static void main(String[] args) {

        // Declare a HashMap with Integer values for Street Numbers & String values for Names:
        //(1000, Liam), (1001, Noah), (1002, Olivia), (1003, Emma), (1004, Benjamin), (1005, Evelyn),  (1006, Lucas)

        HashMap<Integer, String> streetNoName = new HashMap<>();
        streetNoName.put(1000, "Liam");
        streetNoName.put(1001, "Noah");
        streetNoName.put(1002, "Olivia");
        streetNoName.put(1003, "Emma");
        streetNoName.put(1004, "Benjamin");
        streetNoName.put(1005, "Evelyn");
        streetNoName.put(1006, "Lucas");

        //Find the name of the person who lives at 1004 based on the street number
        System.out.println(streetNoName.get(1004));

        //Print out all the odd number streets and its corresponding names
        for (int i : streetNoName.keySet()) {
            if(i % 2 != 0){
                System.out.println(i + ":" + streetNoName.get(i));
            }
        }
    }
}

