import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class Homework11{

    public static void main(String []args){

        List<Integer> list1 = new ArrayList<>();
        Collections.addAll(list1, -2,-3,6,3);
        System.out.println(list1);

        List<Integer> list2 = new ArrayList<>();
        Collections.addAll(list2, -2, -4, 5,-3);
        System.out.println(list2);

        List<Integer> list3 = new ArrayList<>();
        list3.addAll(list1);
        list3.addAll(list2);
        System.out.println(list3);

        int min = Collections.min(list3);
        System.out.println(min);

    }
}