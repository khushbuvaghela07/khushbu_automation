package Homework12;

import java.util.HashSet;
import java.util.TreeSet;


public class Sets {

    public static void main(String[] args) {

        //cohort #1 of the bootcamp: United States, United States, Ukraine, Mexico
        java.util.Set<String> cohort1 = new HashSet<String>();
        cohort1.add("United States");
        cohort1.add("United States");
        cohort1.add("Ukrain");
        cohort1.add("Mexico");

        //cohort #2 of the bootcamp: United States, Canada, United States, Mexico
        java.util.Set<String> cohort2 = new HashSet<String>();
        cohort2.add("United States");
        cohort2.add("Canada");
        cohort2.add("United States");
        cohort2.add("Mexico");

        //Print out the list of all unique countries of every cohort separately (order doesn't matter)
        System.out.println(cohort1);
        System.out.println(cohort2);

        //Print out all unique countries from all cohorts in one line (order doesn't matter)
        java.util.Set<String> cohort3 = new HashSet<>(cohort1);
        cohort3.addAll(cohort2);
        System.out.println(cohort3);

        //Print out all unique countries from all cohorts in alphabetical order
        java.util.Set<String> cohort4 = new TreeSet<>(cohort1);
        cohort4.addAll(cohort2);
        System.out.println(cohort4);

        //Print out the common countries from both cohorts
        java.util.Set<String> cohort5 = new HashSet<>(cohort1);
        cohort5.retainAll(cohort2);
        System.out.println(cohort5);
    }
}
